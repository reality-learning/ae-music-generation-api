from data_util import IndependentNotes
import torch
import torchvision
from torch import nn
from torch.autograd import Variable
import numpy as np

class autoencoder(nn.Module):
    def __init__(self, dataloader):
        hidden_size = 1
        super(autoencoder, self).__init__()
        self.encoder = nn.Sequential(
            nn.Linear(4, 3),
            nn.ReLU(True),
            nn.Linear(3, hidden_size))
        self.decoder = nn.Sequential(
            nn.Linear(hidden_size, 3),
            nn.ReLU(True),
            nn.Linear(3, 4),
            nn.Tanh())

        self.hidden_value = None

        self.counter = 0
        
        self.dataloader = dataloader

    def forward(self, x):
        x = self.encoder(x)
        self.hidden_value = x
        if self.counter % 1000 == 0:
            print("hidden space ", x)
        self.counter += 1
        x = self.decoder(x)
        return x

    def generate_from_hidden(self, x):
        pred = self.decoder(x)
        pred = self.dataloader.denormalized_array(pred.unsqueeze(0))

        midi, duration, velocity, time_to_next = pred.squeeze().detach().numpy()
        return  [{"midi":int(midi), "duration": duration.item(), "velocity": velocity.item(), "time_to_next": time_to_next.item()}]
        
def train_and_save_model():
    num_epochs = 400
    batch_size = 5
    learning_rate = 1e-3

    dataloader = IndependentNotes(batch_size)

    model = autoencoder(dataloader)
    criterion = nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate, weight_decay=1e-5)

    # TRAIN
    inter_cnt = 0
    losses = []
    for epoch in range(num_epochs):
        for data in dataloader:
            batch = torch.tensor(data).float()
            # ===================forward=====================
            output = model(batch)
            loss = criterion(output, batch)
            losses.append(loss.detach().item())
            # ===================backward====================
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            if inter_cnt % 500 == 0:
                print(loss, " epoch ", epoch)
            inter_cnt += 1

        dataloader.reset_iterator()

    torch.save(model.state_dict(), "model_parameters/params")

def get_slot_centers(model, dataloader, num_of_slots):
    hidden_spaces = []
    for row in dataloader.song_ts:
        pred = model(torch.tensor(row).float()).detach().view(1, 4).numpy()
        hidden_spaces.append(model.hidden_value.detach().item())
        pred = dataloader.denormalized_array(pred)
        act = torch.tensor(row).float().view(1, 4).numpy()
        act = dataloader.denormalized_array(act)

    hidden_spaces_pros = np.array(hidden_spaces)
    min_val = hidden_spaces_pros.min()
    max_val = hidden_spaces_pros.max()
    width = max_val - min_val
    slots = np.arange(0,1,1/num_of_slots)
    slot_centers = slots + 1/(num_of_slots*2)
    scaled_centers = (slot_centers * width) + min_val
    return scaled_centers

def get_model_info():
    batch_size = 5
    num_of_slots = 4
    dataloader = IndependentNotes(batch_size)
    model = autoencoder()
    model.load_state_dict(torch.load("model_parameters/params"))
    model.eval()
    slot_info = get_slot_centers(model, dataloader, num_of_slots)
    return model, slot_info
