import torch
import pyro
import pyro.distributions as dist
import torch.nn as nn
import numpy as np
from flask import jsonify

from data_util import DependentNotes
from vae_recurrent.model import Encoder, Decoder, VaeHelper
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

class PyroVaeModel:
    def __init__(self):
        save_model_to = "vae_recurrent/model_parameters/model"
        z_dim = 1

        checkpoint = torch.load(save_model_to)  
        self.decoder = Decoder(z_dim, hidden_dim1=10, hidden_dim2=20, hidden_dim3=10, x_dim = 4)
        self.decoder.load_state_dict(checkpoint['decoder'])

        self.previous_hidden = None
        self.hiddens_in_order = np.load(VaeHelper.mean_hidden_file)

    def generate_from_hidden(self, hidden_val):
        def get_next_hidden(array, value):
            array = np.asarray(array)
            idx = (np.abs(array - value.cpu().detach().numpy())).argmin()

            if idx < self.hiddens_in_order.shape[0] - 1:
                return self.hiddens_in_order[idx + 1]
            else:
                return self.hiddens_in_order[0]

        if self.previous_hidden is None:
            self.previous_hidden = hidden_val

        note_count = np.random.choice(3, 1, p=[0.5,0.3,0.2])[0]
        result_notes = []
        for _ in range(note_count + 1):
            third_tensor = torch.tensor([hidden_val[0], self.previous_hidden[0]])
            x_loc, x_scale = self.decoder(third_tensor)

            pred = dist.Normal(x_loc, x_scale).sample()
            pred = DependentNotes(1).denormalized_array(pred.unsqueeze(0))
            midi, duration, velocity, time_to_next = pred.squeeze().detach().numpy()
            print(int(midi), duration, velocity, time_to_next)

            result_notes.append({"midi":int(midi), "duration": duration.item(), "velocity": velocity.item(), "time_to_next": time_to_next.item()})
            self.previous_hidden = hidden_val
            hidden_val = get_next_hidden(self.hiddens_in_order, hidden_val)
            hidden_val = torch.tensor([hidden_val])

        return result_notes

def get_model_info():
    slot_info = np.load(VaeHelper.user_action_idx_file)
    return PyroVaeModel(), slot_info
